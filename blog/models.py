from django.db import models

from wagtail.core.models import Page
from wagtail.core.fields import StreamField
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, StreamFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.core import blocks as wt_blocks
from .blocks import TitleAndTextBlock
from wagtail.images.blocks import ImageChooserBlock
from wagtail.search import index
from taggit.models import TaggedItemBase
from modelcluster.fields import ParentalKey
from modelcluster.contrib.taggit import ClusterTaggableManager
from wagtail.snippets.edit_handlers import SnippetChooserPanel
from django.utils.translation import gettext as _
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator

from core import blocks as core_blocks



class BlogListPage(Page):
    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        all_posts = BlogPage.objects.live().public().order_by('-first_published_at')

        paginator = Paginator(all_posts, 4)

        page = request.GET.get("page")
        try:
            posts= paginator.page(page)
        
        except PageNotAnInteger:
            posts = paginator.page(1)
        except EmptyPage:
            posts = paginator.page(paginator.num_pages)
        context["posts"] = posts
        return context


class BlogPage(Page):
    # config wagtail
    template = "blog/blog_page.html"
    tags = ClusterTaggableManager(through='BlogPageTag', blank=True)
    # Fields
    subtitle = models.CharField(max_length=120, null=True, blank=True)
    author = models.ForeignKey(
        "core.Collaborators",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,

    )


    # transform to StreamField
    content = StreamField(
        [
            (_("title_and_text"), TitleAndTextBlock()),
            (_("section_bg_70"),core_blocks.SectionBg70Block()),
            (_('heading'), wt_blocks.CharBlock(form_classname="full title")),
            (_('paragraph'), wt_blocks.RichTextBlock()),
            (_('image'), ImageChooserBlock()),
        ],
        null=True,
        blank=True
    )

    image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        # related_name=''
    )

    # Fields models
    content_panels = Page.content_panels + [
        FieldPanel('subtitle', classname="full"),
        FieldPanel("tags"),
        StreamFieldPanel('content'),
        MultiFieldPanel(
            [
                SnippetChooserPanel("author"),
            ],
            heading="Author",
        ),
        ImageChooserPanel('image'),
    ]

    search_fields = Page.search_fields + [
        # ...
        index.SearchField('content_panels'),
        index.SearchField('subtitle'),
    ]

    class Meta:
        verbose_name = _("Blog page")
        verbose_name_plural = _("Blog pages")


class BlogPageTag(TaggedItemBase):
    content_object = ParentalKey(
        'BlogPage',
        related_name='tagged_items',
        on_delete=models.CASCADE,
    )

