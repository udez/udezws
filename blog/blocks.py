"""The place to be for StreamFields"""

from wagtail.core import blocks

class TitleAndTextBlock(blocks.StructBlock):
    title = blocks.CharBlock(required=True)
    text = blocks.RichTextBlock(required=True)

    class Meta:
        template="streams/title_and_text_block.html"
        icon="edit"
        label="Title & Text"