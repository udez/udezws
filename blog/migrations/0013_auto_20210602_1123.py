# Generated by Django 3.1.11 on 2021-06-02 09:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_demopage'),
        ('blog', '0012_remove_blogpage_image_author'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='blogpage',
            name='author_name',
        ),
        migrations.RemoveField(
            model_name='blogpage',
            name='date',
        ),
        migrations.AddField(
            model_name='blogpage',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='core.collaborators'),
        ),
    ]
