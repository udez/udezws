# ROpenExoWebsite

Licence : CC-By-SA 4.0

## Requirements

### System
- [Python 3](https://www.python.org/downloads/)
- [Pipenv](https://realpython.com/pipenv-guide/)

### App
- [Django](https://www.djangoproject.com/)
- [Fontawesom](https://fontawesome.com/icons?d=gallery)
- [Wagtail](https://wagtail.io/)


## Get start
### Clone
`git clone git@gitlab.com:udez/udezws.git`

### Front assets (webpack)
- `npm install`
- `npm run watch`

### Django app
1. `pipenv install`
2. `pipenv run manage.py collectstatic`
2. `pipenv run manage.py migrate`
3. `pipenv run manage.py runserver`
4. `pipenv run manage.py createsuperuser`


_Penser à utiliser l'extension python de VSCode qui permet d'activer directement l'environnement virtuel_

## Export data
`./manage.py dumpdata --natural-foreign --indent 2 -e contenttypes -e auth.permission -e wagtailcore.groupcollectionpermission -e wagtailcore.grouppagepermission -e wagtailimages.rendition -e sessions -e wagtail_localize > data.json`
`python .\manage.py dumpdata > export.json`

## Export data
`python .\manage.py loaddata export.json`


## Deploy with heroku
`heroku login`
`heroku create`
`heroku buildpacks:add --index 1 heroku/nodejs`
`heroku buildpacks:add --index 2 heroku/python`
`git push heroku main`
`heroku config:set DJANGO_SETTINGS_MODULE='udezws.settings.production`

More info just [here](https://devcenter.heroku.com/articles/git)

### Deploy it yourself
[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)


## Others
### Usinig Emmet with vscode
[Cheat-sheet](https://docs.emmet.io/cheat-sheet/)