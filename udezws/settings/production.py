from .base import *
import django_heroku

DEBUG = os.environ.setdefault("DEBUG", False)
django_heroku.settings(locals())


try:
    from .local import *
except ImportError:
    pass
