# common blocks
from wagtail.core import blocks
from wagtail.core.templatetags.wagtailcore_tags import richtext
from wagtail.images.blocks import ImageChooserBlock
from wagtailsvg.blocks import SvgChooserBlock
from django.utils.translation import gettext as _

class LinkStructValue(blocks.StructValue):
    """Additional logic for our urls."""

    def url(self):
        button_page = self.get(_('button_page'))
        button_url = self.get(_('button_url'))
        if button_page:
            return button_page.url
        elif button_url:
            return button_url

        return None

    # def latest_posts(self):
    #     return BlogDetailPage.objects.live()[:3]


class ButtonBlock(blocks.StructBlock):
    """An external or internal URL."""

    title = blocks.CharBlock(required=True, max_length=60)
    button_page = blocks.PageChooserBlock(required=False, help_text=_('If selected, this url will be used first'))
    button_url = blocks.URLBlock(required=False, help_text=_('If added, this url will be used secondarily to the button page'))

    # def get_context(self, request, *args, **kwargs):
    #     context = super().get_context(request, *args, **kwargs)
    #     context['latest_posts'] = BlogDetailPage.objects.live().public()[:3]
    #     return context

    class Meta:  # noqa
        template = "streams/button_block.html"
        icon = "placeholder"
        label = _("Single Button")
        value_class = LinkStructValue


class SectionBg70Block(blocks.StructBlock):
    """An external or internal URL."""

    title = blocks.CharBlock(required=True, max_length=60)
    subtitle = blocks.CharBlock(required=True, max_length=60)
    content_text = blocks.TextBlock(required=True,)

    # def get_context(self, request, *args, **kwargs):
    #     context = super().get_context(request, *args, **kwargs)
    #     context['latest_posts'] = BlogDetailPage.objects.live().public()[:3]
    #     return context

    class Meta:  # noqa
        template = "streams/section_bg_70_block.html"
        icon = "pen"
        label = _("Section couleur primaire 70%")


class ConstatBlock(blocks.StructBlock):
    """An external or internal URL."""
    image = SvgChooserBlock()
    title = blocks.CharBlock(required=True, max_length=60)
    content_text = blocks.RichTextBlock(required=True, features=['bold', 'italic'])

    class Meta:  # noqa
        template = "streams/constat_block.html"
        icon = "pen"
        label =_ ("1 Constat")
        block_counts = {
            'heading': {'min_num': 1, 'max_num': 3},
        }


class SectionConstatBlock(blocks.StructBlock):
    """An external or internal URL."""

    title = blocks.CharBlock(required=True, max_length=60)
    description = blocks.RichTextBlock(required=True, features=['bold', 'italic'])
    constats = blocks.StreamBlock([
            ('constat', ConstatBlock(),)
        ], min_num=1, max_num=3)

    class Meta:  # noqa
        template = "streams/section_constat_block.html"
        icon = "pen"
        label = _("Section constat")

class InnovationBlock(blocks.StructBlock):
    """An external or internal URL."""

    title_innovation = blocks.CharBlock(required=True, max_length=60)
    content_text = blocks.RichTextBlock(required=True, features=['bold', 'italic'])

    class Meta:  # noqa
        template = "streams/innovation_block.html"
        icon = "pen"
        label = _("1 Innovation")
        block_counts = {
            'heading': {'min_num': 1, 'max_num': 3},
        }

class SectionInnovationBlock(blocks.StructBlock):
    """An external or internal URL."""

    description = blocks.RichTextBlock(required=True, features=['bold', 'italic'])
    innovations = blocks.StreamBlock([
            ('innovation', ConstatBlock(),)
        ], min_num=1, max_num=3)

    class Meta:  # noqa
        template = "streams/section_innovation_block.html"
        icon = "pen"
        label = _("Section Innovation")