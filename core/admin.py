from wagtail.contrib.modeladmin.options import(
    ModelAdmin,
    modeladmin_register,
)
from .models import Collaborators

class CollaboratorsAdmin(ModelAdmin):
    model = Collaborators
    menu_label ="OurTeams"
    menu_icon ="placeholder"
    menu_order = 290
    add_to_settings_menu = False
    exclude_from_explorer = False
    list_display = ("lastname", "username", "fonction", "description", "image",)
    search_fields = ("lastname", "usename", "fonction", "description", "image",)

modeladmin_register(CollaboratorsAdmin)
