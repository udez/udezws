from django.db import models
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.core.models import Page
from wagtail.snippets.models import register_snippet




class Collaborators(models.Model):
    
    firstname = models.CharField(max_length=100, blank=False, null=False, help_text="Prenom")
    lastname = models.CharField(max_length=100, blank=False, null=False, help_text="Nom")
    username = models.CharField(max_length=100, blank=False, null=False, help_text="Pseudo")
    fonction = models.CharField(max_length=100, blank=False, null=False, help_text="Fonction")
    description = models.CharField(max_length=100, blank=False, null = False, help_text="Description")
    is_external = models.BooleanField(default=False)
    image =  models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.SET_NULL,
        null=True,
        blank=False,
        related_name="+",
    )


    panels=[
        MultiFieldPanel(
            [
                FieldPanel("firstname"),
                FieldPanel("lastname"),
                FieldPanel("username"),
                FieldPanel("fonction"),
                FieldPanel("description"),
                ImageChooserPanel("image"),
                FieldPanel("is_external"),
            ],
            heading="Team"
        )
    ]

    def __str__(self):
        return self.lastname


register_snippet(Collaborators)


class DemoPage(Page):
    # config wagtail
    max_count = 1
    template = "core/demo_page.html"
    class Meta:
        verbose_name = "Demo page"
        verbose_name_plural = "Demo pages"