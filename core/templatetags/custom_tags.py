from django import template

register = template.Library()

@register.inclusion_tag('templatetags/main_menu_items.html')
def main_menu_items(request_path, menu_items):
    return {
        'menu_items': menu_items,
        'request_path': request_path
    }


@register.inclusion_tag('templatetags/bread_crumbs_navigation.html')
def bread_crumbs_navigation(page):
    ancestors = page.get_ancestors()
    if len(ancestors) > 2:
        return {
            'ancestors': ancestors,
            'page_title': page.title
        }


@register.inclusion_tag('templatetags/card_items.html')
def card_items(title, descrpition, image):
    return {
        'title_card': title,
        'descrpition_card': descrpition,
        'image': image
    }

@register.inclusion_tag('templatetags/social_button.html')
def get_social_media_button(social_media_name, url_post, title_post):
    # fontawesome icons
    social_media_links = {
        'facebook' : {
            'url' : 'https://www.facebook.com/sharer.php?u={url_post}&t={title_post}',
            'icon': 'fa-facebook'
        },
        'linkedin' : {
            'url' : 'https://www.linkedin.com/shareArticle?mini=true&url={url_post}&title={title_post}',
            'icon': 'fa-linkedin'
            
        },
        'twitter' : {
            'url' : 'https://twitter.com/share?url={url_post}&text={title_post}&via=ROpenExo',
            'icon': 'fa-twitter-square'
        }
    }
    return {
        'url': social_media_links[social_media_name]['url'].format(url_post=url_post, title_post=title_post),
        'icon': social_media_links[social_media_name]['icon']
    }