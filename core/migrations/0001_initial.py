# Generated by Django 3.1.11 on 2021-05-21 14:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('wagtailimages', '0023_add_choose_permissions'),
    ]

    operations = [
        migrations.CreateModel(
            name='OurTeam',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('last_name_team', models.CharField(help_text='Nom', max_length=100)),
                ('surname_team', models.CharField(help_text='Prenom', max_length=100)),
                ('fonction_team', models.CharField(help_text='Fonction', max_length=100)),
                ('description_team', models.CharField(help_text='Description', max_length=100)),
                ('image', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.image')),
            ],
        ),
    ]
