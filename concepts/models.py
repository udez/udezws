from django.db import models

from wagtail.core.models import Page, Orderable
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from wagtail.core.fields import StreamField
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel, StreamFieldPanel, RichTextField
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.documents.edit_handlers import DocumentChooserPanel
from wagtail.snippets.edit_handlers import SnippetChooserPanel
from wagtail.snippets.models import register_snippet
from django.utils.translation import gettext as _
from modelcluster.fields import ParentalKey
from wagtail.core import blocks as wt_blocks
from wagtail.images.blocks import ImageChooserBlock




class ConceptListPage(Page):

    parent_page_types = ['home.HomePage']
    subpage_types = ['concepts.ConceptPage']

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        all_posts = ConceptPage.objects.live().public().order_by('-first_published_at')

        paginator = Paginator(all_posts, 4)

        page = request.GET.get("page")
        try:
            posts= paginator.page(page)
        
        except PageNotAnInteger:
            posts = paginator.page(1)
        except EmptyPage:
            posts = paginator.page(paginator.num_pages)
        context["posts"] = posts
        return context



class DesignStatus(models.IntegerChoices):
    IDEA = 0, _('Idée')
    CONCEPT = 1, _('Concept')
    PROTOTYPE = 2, _('Prototype')
    STABLE = 3, _('Stable')

    __empty__ = _('(Unknown)')


class Concept(models.Model):
    title = models.CharField(max_length=100, blank=False, null=False, help_text=_("Prenom"))
    version = models.CharField(max_length=8, blank=False, null=False, help_text=_("Nom"))
    status = models.IntegerField(help_text=_("Design status"), choices=DesignStatus.choices)
    is_public = models.BooleanField(default=False)
    image =  models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.SET_NULL,
        null=True,
        blank=False,
        related_name="+",
    )

    panels=[
        MultiFieldPanel(
            [
                FieldPanel("title"),
                FieldPanel("version"),
                FieldPanel("status"),
                FieldPanel("is_public"),
                ImageChooserPanel("image"),
            ],
            heading="Concept"
        )
    ]

    def __str__(self):
        return self.title


register_snippet(Concept)


class ConceptPage(Page):
    # config wagtail
    template = "concepts/concept_page.html"
    subpage_types = ['HowToMakePage']
    # Fields
    subtitle = models.CharField(max_length=120, null=True, blank=True)
    concept = models.ForeignKey(
        Concept,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    # transform to StreamField
    description_header = RichTextField()

    image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        # related_name=''
    )

    # Fields models
    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                SnippetChooserPanel("concept"),
            ],
            heading="Concept",
        ),
        FieldPanel('subtitle', classname="full"),
        FieldPanel("description_header"),
        ImageChooserPanel('image'),
    ]

    def get_tutorials(self):
        # import ipdb ; ipdb.set_trace()
        return self.get_children().live()
    class Meta:
        verbose_name = _("Concept page")
        verbose_name_plural = _("Concept pages")


class HowToMakePage(Page):
    # config wagtail
    template = "concepts/how_to_make_page.html"
    # Fields
    sources_files =models.ForeignKey(
       'wagtaildocs.Document',
       null=True,
       blank=True,
       on_delete=models.SET_NULL,
       related_name='+'
    )
    description_header = RichTextField(blank=True, null=True)

    image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        related_name='preview_image'
    )
    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                ImageChooserPanel('image'),
                FieldPanel("description_header"),
                DocumentChooserPanel("sources_files"),
            ],
            heading="Infos",
            classname="collapsible"
        ),
        MultiFieldPanel([
            InlinePanel('tutorial_steps')
        ], heading=_("Tutotial steps"), classname="collapsible collapsed"),
    ]


class TutorialSteps(Orderable):
    name = models.CharField(max_length=120, null=True, blank=True)
    content = StreamField(
        [
            (_('heading'), wt_blocks.CharBlock(form_classname="full title")),
            (_('paragraph'), wt_blocks.RichTextBlock()),
            (_('image'), ImageChooserBlock()),
        ],
        null=True,
        blank=True
    )
    page = ParentalKey("HowToMakePage", related_name="tutorial_steps")

    panels = [
        FieldPanel('name'),
        StreamFieldPanel("content"),
    ]

    @property
    def link(self):
        if self.link_page:
            return self.link_page.url
        elif self.link_url:
            return self.link_url
        return '#'

    @property
    def title(self):
        if self.link_page and not self.link_title:
            return self.link_page.title
        elif self.link_title:
            return self.link_title
        return 'Missing Title'


