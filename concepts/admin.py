from wagtail.contrib.modeladmin.options import(
    ModelAdmin,
    modeladmin_register,
)
from .models import Concept

class ConceptAdmin(ModelAdmin):
    model = Concept
    menu_label ="Concepts"
    menu_icon ="placeholder"
    menu_order = 260
    add_to_settings_menu = False
    exclude_from_explorer = False
    list_display = ("title", "version", "status", "image",)
    search_fields = ("title", "version", )

modeladmin_register(ConceptAdmin)
