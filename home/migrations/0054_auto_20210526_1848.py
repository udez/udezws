# Generated by Django 3.1.11 on 2021-05-26 16:48

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields
import wagtailsvg.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0053_auto_20210526_1246'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='homepage',
            name='description_constat1',
        ),
        migrations.RemoveField(
            model_name='homepage',
            name='description_constat2',
        ),
        migrations.RemoveField(
            model_name='homepage',
            name='description_constat3',
        ),
        migrations.RemoveField(
            model_name='homepage',
            name='title_constat1',
        ),
        migrations.RemoveField(
            model_name='homepage',
            name='title_constat2',
        ),
        migrations.RemoveField(
            model_name='homepage',
            name='title_constat3',
        ),
        migrations.AlterField(
            model_name='homepage',
            name='content',
            field=wagtail.core.fields.StreamField([('section_bg_70', wagtail.core.blocks.StructBlock([('title', wagtail.core.blocks.CharBlock(max_length=60, required=True)), ('subtitle', wagtail.core.blocks.CharBlock(max_length=60, required=True)), ('content_text', wagtail.core.blocks.TextBlock(required=True))])), ('section_constat', wagtail.core.blocks.StructBlock([('title', wagtail.core.blocks.CharBlock(max_length=60, required=True)), ('description', wagtail.core.blocks.RichTextBlock(features=['bold', 'italic'], required=True)), ('constats', wagtail.core.blocks.StreamBlock([('constat', wagtail.core.blocks.StructBlock([('image', wagtailsvg.blocks.SvgChooserBlock()), ('title', wagtail.core.blocks.CharBlock(max_length=60, required=True)), ('content_text', wagtail.core.blocks.RichTextBlock(features=['bold', 'italic'], required=True))]))], max_num=3, min_num=1))])), ('section_innovation', wagtail.core.blocks.StructBlock([('description', wagtail.core.blocks.RichTextBlock(features=['bold', 'italic'], required=True)), ('innovations', wagtail.core.blocks.StreamBlock([('innovation', wagtail.core.blocks.StructBlock([('title_innovation', wagtail.core.blocks.CharBlock(max_length=60, required=True)), ('content_text', wagtail.core.blocks.RichTextBlock(features=['bold', 'italic'], required=True))]))], max_num=3, min_num=1))])), ('our_team', wagtail.core.blocks.StructBlock([('title', wagtail.core.blocks.CharBlock(max_length=60, required=True)), ('subtitle', wagtail.core.blocks.CharBlock(max_length=60, required=False))])), ('collabs', wagtail.core.blocks.StructBlock([('title', wagtail.core.blocks.CharBlock(max_length=60, required=True)), ('subtitle', wagtail.core.blocks.CharBlock(max_length=60, required=False))]))], blank=True, null=True),
        ),
    ]
