# Generated by Django 3.1.11 on 2021-05-17 11:07

from django.db import migrations, models
import django.db.models.deletion
import wagtail.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0006_socialmediasettings'),
    ]

    operations = [
        migrations.CreateModel(
            name='GenericPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.page')),
                ('content', wagtail.core.fields.RichTextField(blank=True)),
            ],
            options={
                'verbose_name': 'Generic page',
                'verbose_name_plural': 'Generic pages',
            },
            bases=('wagtailcore.page',),
        ),
    ]
