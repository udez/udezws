# Generated by Django 3.1.11 on 2021-05-20 14:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0032_cookiespage_cookies_used'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccessibilityPage',
            fields=[
                ('genericpage_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='home.genericpage')),
            ],
            options={
                'abstract': False,
            },
            bases=('home.genericpage',),
        ),
        migrations.RemoveField(
            model_name='homepage',
            name='paragraph_our_debut',
        ),
        migrations.RemoveField(
            model_name='homepage',
            name='paragraph_team',
        ),
        migrations.RemoveField(
            model_name='homepage',
            name='team_subtitle',
        ),
        migrations.RemoveField(
            model_name='homepage',
            name='team_title',
        ),
    ]
