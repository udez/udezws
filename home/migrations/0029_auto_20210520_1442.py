# Generated by Django 3.1.11 on 2021-05-20 12:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0028_auto_20210520_1145'),
    ]

    operations = [
        migrations.AddField(
            model_name='companysettings',
            name='company_adresse',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AddField(
            model_name='companysettings',
            name='company_mail',
            field=models.CharField(blank=True, max_length=60),
        ),
        migrations.AddField(
            model_name='companysettings',
            name='company_name',
            field=models.CharField(blank=True, max_length=120),
        ),
        migrations.AddField(
            model_name='companysettings',
            name='company_siret',
            field=models.CharField(blank=True, max_length=20),
        ),
        migrations.AddField(
            model_name='companysettings',
            name='company_telephone_number_company',
            field=models.CharField(blank=True, max_length=15),
        ),
        migrations.AddField(
            model_name='companysettings',
            name='hebergeur_adresse',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AddField(
            model_name='companysettings',
            name='hebergeur_mail',
            field=models.CharField(blank=True, max_length=60),
        ),
        migrations.AddField(
            model_name='companysettings',
            name='hebergeur_name',
            field=models.CharField(blank=True, max_length=120),
        ),
        migrations.AddField(
            model_name='companysettings',
            name='hebergeur_siret',
            field=models.CharField(blank=True, max_length=20),
        ),
        migrations.AddField(
            model_name='companysettings',
            name='hebergeur_telephone_number',
            field=models.CharField(blank=True, max_length=15),
        ),
        migrations.CreateModel(
            name='DataTreatementSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_treatement_name', models.CharField(blank=True, max_length=120)),
                ('data_treatement_adresse', models.CharField(blank=True, max_length=200)),
                ('data_treatement_telephone_number', models.CharField(blank=True, max_length=15)),
                ('data_treatement_mail', models.CharField(blank=True, max_length=60)),
                ('data_treatement_siret', models.CharField(blank=True, max_length=20)),
                ('controller_name', models.CharField(blank=True, max_length=120)),
                ('controller_adresse', models.CharField(blank=True, max_length=200)),
                ('controller_telephone_number', models.CharField(blank=True, max_length=15)),
                ('controller_mail', models.CharField(blank=True, max_length=60)),
                ('controller_siret', models.CharField(blank=True, max_length=20)),
                ('site', models.OneToOneField(editable=False, on_delete=django.db.models.deletion.CASCADE, to='wagtailcore.site')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
