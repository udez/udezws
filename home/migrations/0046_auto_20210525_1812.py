# Generated by Django 3.1.11 on 2021-05-25 16:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0045_delete_constat'),
    ]

    operations = [
        migrations.AddField(
            model_name='homepage',
            name='description_constat1',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AddField(
            model_name='homepage',
            name='description_constat2',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AddField(
            model_name='homepage',
            name='description_constat3',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AddField(
            model_name='homepage',
            name='title_constat1',
            field=models.CharField(blank=True, max_length=120),
        ),
        migrations.AddField(
            model_name='homepage',
            name='title_constat2',
            field=models.CharField(blank=True, max_length=120),
        ),
        migrations.AddField(
            model_name='homepage',
            name='title_constat3',
            field=models.CharField(blank=True, max_length=120),
        ),
    ]
