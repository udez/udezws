# Generated by Django 3.1.11 on 2021-05-19 09:29

from django.db import migrations
import wagtail.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0020_auto_20210519_1039'),
    ]

    operations = [
        migrations.AddField(
            model_name='homepage',
            name='paragraph_our_debut',
            field=wagtail.core.fields.RichTextField(blank=True),
        ),
    ]
