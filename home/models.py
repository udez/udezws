from django.db import models
from django.db.models.base import Model
from django.db.models.deletion import SET_NULL
from django.db.models.fields import related
from wagtail.core.hooks import register

from wagtail.core.models import Page
from wagtail.core.fields import RichTextField, StreamField
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel,StreamFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.core import blocks as wt_blocks
from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.snippets.edit_handlers import SnippetChooserPanel
from wagtail.snippets.models import register_snippet
from django.utils.translation import gettext as _


from core import blocks as core_blocks
from home import blocks as home_blocks


class HomePage(Page):
    # config wagtail
    max_count = 1
    template = "home/home_page.html"

    # Fields
    banner_title = models.CharField(default="Les gestes du quotidien pour toutes et tous !", max_length=120)
    banner_subtitle = RichTextField(blank=True)
    content = StreamField(
        [
            ("section_bg_70",core_blocks.SectionBg70Block()),
            ("section_constat",core_blocks.SectionConstatBlock()),
            ("section_innovation",core_blocks.SectionInnovationBlock()),
            ("our_team",home_blocks.OurTeamBlock()),
            ("collabs",home_blocks.OurCollabBlock()),
        ],
        null=True,
        blank=True,
    )
    banner_image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        # related_name=''
    )
    banner_cta = models.ForeignKey(
        "wagtailcore.Page",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='banner_link'
    )
    
    # Fields models
    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel('banner_title', classname="full"),
                FieldPanel('banner_subtitle', classname="full"),
                ImageChooserPanel('banner_image'),
                SnippetChooserPanel("banner_cta")
            ], 
            heading=_("Banner"),
            classname="collapsible collapsed"
        ),

        StreamFieldPanel("content"),
    ]

    class Meta:
        verbose_name = _("Home page")
        verbose_name_plural = "Home pages"

@register_setting
class SocialMediaSettings(BaseSetting):
    linkedin = models.URLField(default="#", blank=True, null=True, help_text="LinkedIn")
    twitter = models.URLField(default="#", blank=True, null=True, help_text="Twitter")
    youtube = models.URLField(default="#", blank=True, null=True, help_text="Youtube")

    panels = [
        MultiFieldPanel([
            FieldPanel('linkedin'),
            FieldPanel('twitter'),
            FieldPanel('youtube'),
        ], heading="Social Media")
    ]
    
class GenericPage(Page):
    # config wagtail
    # max_count = 1
    template = "home/generic_page.html"

    # Fields
    
    content = StreamField(
        [
            ('paragraph', wt_blocks.RichTextBlock()),
        ],
        null=True,
        blank=True
    
    )
    # Fields models
    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [        
                StreamFieldPanel('content', classname="full"),
            ],
            heading=_("Extra"),
            classname="collapsible collapsed"
        ),
    ]

    class Meta:
        verbose_name = "Generic page"
        verbose_name_plural = "Generic pages"


class MentionLegalPage(GenericPage):
#     # config wagtail
#     # max_count = 1
    template = "home/mentionlegal_page.html"


class ConfidentialitePage(GenericPage):

    template = "home/confidentialite_page.html"

    purpose = RichTextField(blank=True)
    legal_basis = RichTextField(blank=True)
    personal_data_processed = RichTextField(blank=True)
    data_recipient = RichTextField(blank=True)
    data_storage = RichTextField(blank=True)
    data_protection = RichTextField(blank=True)
    client_right = RichTextField(blank=True)
    cnil = RichTextField(blank=True)





    content_panels = Page.content_panels + [
        FieldPanel('purpose', classname="full"),
        FieldPanel('legal_basis', classname="full"),
        FieldPanel('personal_data_processed', classname="full"),
        FieldPanel('data_recipient', classname="full"),
        FieldPanel('data_storage', classname="full"),
        FieldPanel('data_protection', classname="full"),
        FieldPanel('client_right', classname="full"),
        FieldPanel('cnil', classname="full"),
    ]


class CookiesPage(GenericPage):

    template= "home/cookies_page.html"

    cookies_used = RichTextField(blank =True)

    content_panels = Page.content_panels+[

        FieldPanel('cookies_used', classname='full')


    ]
class AccessibilityPage(GenericPage):

    template = "home/accessibility_page.html"



@register_setting
class CompanySettings(BaseSetting):
    company_name = models.CharField(blank=True, max_length=120)
    company_adresse = models.CharField(blank=True, max_length=200)
    company_telephone_number_company = models.CharField(blank=True, max_length=15)
    company_mail = models.CharField(blank=True, max_length=60)
    company_siret= models.CharField(blank=True, max_length=20)
    hebergeur_name = models.CharField(blank=True, max_length=120)
    hebergeur_adresse = models.CharField(blank=True, max_length=200)
    hebergeur_telephone_number = models.CharField(blank=True, max_length=15)
    hebergeur_mail = models.CharField(blank=True, max_length=60)
    hebergeur_siret = models.CharField(blank=True, max_length=20)

    panels = [
        MultiFieldPanel(
            [
                FieldPanel(_('company_name'), classname="full"),
                FieldPanel(_('company_adresse'), classname="full"),
                FieldPanel(_('company_telephone_number_company'), classname="full"),
                FieldPanel(_('company_mail'), classname="full"),
                FieldPanel(_('company_siret'), classname="full"),
            ],
            heading=_("Votre entreprise"),
            classname="collapsible"
        ),
        MultiFieldPanel(
            [
                FieldPanel(_('hebergeur_name'), classname="full"),
                FieldPanel(_('hebergeur_adresse'), classname="full"),
                FieldPanel(_('hebergeur_telephone_number'), classname="full"),
                FieldPanel(_('hebergeur_mail'), classname="full"),
                FieldPanel(_('hebergeur_siret'), classname="full"),
            ],
            heading=_("Votre hebergeur"),
            classname="collapsible collapsed"
        ),
    ]

@register_setting
class DataTreatementSettings(BaseSetting):
    data_treatement_name = models.CharField(blank=True, max_length=120)
    data_treatement_adresse = models.CharField(blank=True, max_length=200)
    data_treatement_telephone_number = models.CharField(blank=True, max_length=15)
    data_treatement_mail = models.CharField(blank=True, max_length=60 )
    data_treatement_siret = models.CharField(blank=True, max_length=20)
    controller_name = models.CharField(blank=True, max_length=120)
    controller_adresse = models.CharField(blank=True, max_length=200)
    controller_telephone_number = models.CharField(blank=True, max_length=15)
    controller_mail = models.CharField(blank=True, max_length=60)
    controller_siret = models.CharField(blank=True, max_length=20)
    

    


    panels = [
        MultiFieldPanel(
            [
                FieldPanel(_('data_treatement_name'), classname="full"),
                FieldPanel(_('data_treatement_adresse'), classname="full"),
                FieldPanel(_('data_treatement_telephone_number'), classname="full"),
                FieldPanel(_('data_treatement_siret'), classname="full"),
            ],
            heading=_("Entreprise du traitement des données"),
            classname="collapsible collapsed"
        ),
        MultiFieldPanel(
            [
                FieldPanel(_('controller_name'), classname="full"),
                FieldPanel(_('controller_adresse'), classname="full"),
                FieldPanel(_('controller_telephone_number'), classname="full"),
                FieldPanel(_('controller_mail'), classname="full"),
                FieldPanel(_('controller_siret'), classname="full"),
            ],
            heading=_("Responsable des données"),
            classname="collapsible collapsed"
        ),
    ]
