# common blocks
from core.models import Collaborators
from wagtail.core import blocks


class OurTeamBlock(blocks.StructBlock):
    """An external or internal URL."""

    title = blocks.CharBlock(required=True, max_length=60)
    subtitle = blocks.CharBlock(required=False, max_length=60)

    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)
        context['our_team'] = Collaborators.objects.filter(is_external = False)
        return context

    class Meta:  # noqa
        template = "streams/our_team_block.html"
        icon = "pen"
        label = "Our Team"
    
class OurCollabBlock(blocks.StructBlock):
    """An external or internal URL."""

    title = blocks.CharBlock(required=True, max_length=60)
    subtitle = blocks.CharBlock(required=False, max_length=60)

    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)
        context['our_team'] = Collaborators.objects.filter(is_external = True)
        return context

    class Meta:  # noqa
        template = "streams/our_collaborators_block.html"
        icon = "pen"
        label = "Collaborators"

