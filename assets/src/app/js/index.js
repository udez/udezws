const initEvent = (className, type, callback) => {
    var items = document.getElementsByClassName(className);
    for (var i = 0; i < items.length; i++) {
        items[i].addEventListener(type, callback);
    }
}

initEvent('dropdown', 'click', event => {
    event.stopPropagation();
    event.currentTarget.classList.toggle('is-active');
});

function open_share_popup(event) {
    window.open(this.dataset.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=500,width=700');
    event.stopPropagation();
    return false;
}

initEvent('btn_social_media_share', 'click', open_share_popup);
initEvent('collapse-btn', 'click', event => {
    // debugger
    var btnTarget = event.currentTarget;
    let target = btnTarget.dataset['target'];
    let elmtTarget = document.getElementById(target);
    if (elmtTarget.classList.contains("show")) {
        elmtTarget.classList.remove("show");
        btnTarget.children[0].children[0].classList.remove("fa-angle-up");
        btnTarget.children[0].children[0].classList.add("fa-angle-down");
    } else {
        elmtTarget.classList.add("show");
        btnTarget.children[0].children[0].classList.remove("fa-angle-down");
        btnTarget.children[0].children[0].classList.add("fa-angle-up");
    }
});

// for (var i = 0; i < btns_social_media.length; i++) {
//     btns_social_media[i].addEventListener('click', open_share_popup, false);
// }

var viewer;
var options = {
    env: 'AutodeskProduction',
    api: 'derivativeV2_EU', // for models uploaded to EMEA change this option to 'derivativeV2_EU'
    getAccessToken: function(onTokenReady) {
        var token = 'x8TMQws0Hr05vD4A';
        var timeInSeconds = 3600; // Use value provided by Forge Authentication (OAuth) API
        onTokenReady(token, timeInSeconds);
    }
};
// Autodesk.Viewing.Initializer(options, function() {

//     var htmlDiv = document.getElementById('forgeViewer');
//     viewer = new Autodesk.Viewing.GuiViewer3D(htmlDiv);
//     var startedCode = viewer.start();
//     if (startedCode > 0) {
//         console.error('Failed to create a Viewer: WebGL not supported.');
//         return;
//     }

//     console.log('Initialization complete, loading a model next...');

// });

// var documentId = "urn:dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6dWRlel8yMDIxLTA3LTIzMi9ST0VfMC42XzIxMDUwM18xLlNURVA=";
// Autodesk.Viewing.Document.load(documentId, onDocumentLoadSuccess, onDocumentLoadFailure);

// function onDocumentLoadSuccess(viewerDocument) {
//     // viewerDocument is an instance of Autodesk.Viewing.Document
//     console.log("yes");
// }

// function onDocumentLoadFailure() {
//     console.error('Failed fetching Forge manifest');
// }