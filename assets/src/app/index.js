// entry point for webpack
import "./styles/main.scss";
// import "./js/index.js";
import Alpine from 'alpinejs'

window.Alpine = Alpine;

let Dropdown = () => ({
    open: false,
    btn: {
        ['@click']() {
            this.open = !this.open;
        }
    },
    content: {
        ['x-show']() {
            return this.open;
        },
        ['@click.outside']() {
            this.open = false
        }
    }
});

document.addEventListener('alpine:init', () => {
    Alpine.data('dropdown', Dropdown);
});

Alpine.start();