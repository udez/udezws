const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

function recursiveIssuer(m, c) {
    const issuer = c.moduleGraph.getIssuer(m);
    // For webpack@4 issuer = m.issuer

    if (issuer) {
        return recursiveIssuer(issuer, c);
    }

    const chunks = c.chunkGraph.getModuleChunks(m);
    // For webpack@4 chunks = m._chunks

    for (const chunk of chunks) {
        return chunk.name;
    }

    return false;
}

module.exports = {
    // entry: path.resolve(__dirname, "assets/src/index.js"),
    entry: {
        app: path.resolve(__dirname, 'assets/src/app/index.js'),
        backend: path.resolve(__dirname, 'assets/src/backend/index.js'),
    },
    output: {
        filename: "[name].js",
        path: path.resolve(__dirname, "assets/dist")
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                fooStyles: {
                    name: 'styles_app',
                    test: (m, c, entry = 'app') =>
                        m.constructor.name === 'CssModule' &&
                        recursiveIssuer(m, c) === entry,
                    chunks: 'all',
                    enforce: true,
                },
                barStyles: {
                    name: 'styles_backend',
                    test: (m, c, entry = 'backend') =>
                        m.constructor.name === 'CssModule' &&
                        recursiveIssuer(m, c) === entry,
                    chunks: 'all',
                    enforce: true,
                },
            },
        },
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].css',
        })
    ],
    module: {
        rules: [{
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader, // 3. Recrée les fichiers css à partir de leurs équivalents en Javascript.
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                            // options...
                        }
                    }
                ],
                exclude: /node_modules/
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                }, ],
            }
        ]
    }
};